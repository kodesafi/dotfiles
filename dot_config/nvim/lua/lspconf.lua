require('lspconfig')

local lsp_installer = require("nvim-lsp-installer")

local servers = {}

for _,name in pairs(servers) do 
  local found_server, server = lsp_installer.getserver(name)
  
  if found_server and not server:is_installed() then 
    print("Installing " .. name)
    server:install()
  end
end