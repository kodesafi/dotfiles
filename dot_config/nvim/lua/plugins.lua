return {  
  {
    'navarasu/onedark.nvim',
    config = function()
      require('onedark').setup {
        style = "warmer",
  
        colors = {
          bg0 = "#121212",
        },
      }
      require('onedark').load()
    end,
  },

  {
    'nvim-treesitter/nvim-treesitter',
    build = function()
      local ts_update = require('nvim-treesitter.install').update({
        with_sync = true
      })
    end,
    
    config = function()
      require'nvim-treesitter.configs'.setup {
        -- ensure_installed = { "norg" },
        
        highlight = {
          enable = true,
        }
      }
    end,

    dependencies = {
      'nvim-treesitter/playground'
    },
  },

  {
    "nvim-neorg/neorg",
    build = ":Neorg sync-parsers",
    config = function()
      require('neorg').setup {
        load = {
          ["core.defaults"] = {},
          ["core.concealer"] = {},
        }
      }
    end,

    dependencies = { "nvim-lua/plenary.nvim" }
  },

  {
    'vim-scripts/auto-pairs-gentle'
  },

  {
    'nvim-lualine/lualine.nvim',
    dependencies = {
      'kyazdani42/nvim-web-devicons',
      'arkav/lualine-lsp-progress',
    },
  },

  {
    'neovim/nvim-lspconfig'
  },

  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
    }
  },
}
