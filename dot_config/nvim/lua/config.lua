vim.wo.relativenumber = true
vim.o.showmode = false
vim.o.autowriteall = true
vim.opt.list = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.updatetime = 250
vim.wo.signcolumn = 'yes'
vim.o.termguicolors = true
vim.o.completeopt = 'menu,menuone'
vim.g.mapleader = " "

vim.cmd [[
  augroup YankHighlight 
  autocmd! 
  autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]]

