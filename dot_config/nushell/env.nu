# Nushell Environment Config File

def jump_dir () {
    fd . --full-path $env.HOME -d 4 -t d 
    | fzf --height 15 --no-separator --preview "^ls {1}" --preview-window border-left
}

alias q = exit
alias h = cd ~
alias cz = chezmoi
alias lsal = ls -al
alias icat = kitty +kitten icat

# alias nzig = nix run 'github:mitchellh/zig-overlay#master-2023-05-01'
alias jd = cd (jump_dir)

let-env RUSTUP_HOME = $"($env.HOME)/.local/rustup"
let-env CARGO_HOME = $"($env.HOME)/.local/cargo"
let-env GOPATH = $"($env.HOME)/.local/go"
let-env LD_LIBRARY_PATH = "/run/opengl-driver/lib:/nix/var/nix/profiles/system/sw/lib"

let-env TZ = "Africa/Nairobi"

let-env EDITOR = "hx"

# let-env XDG_RUNTIME_DIR = "/tmp/wayland"
let-env XKB_DEFAULT_OPTIONS = "caps:escape"

let-env PATH = (
    $env.PATH 
    | append $"($env.HOME)/.local/bin"
    | append $"($env.HOME)/.local/go/bin"
    | append $"($env.HOME)/.local/cargo/bin"
    | append $"($env.HOME)/.local/rustup/bin"
)

def create_left_prompt [] { }

def create_right_prompt [] {
    let path_segment = ($env.PWD  | split row '/' | last 2 | str join '/') 
    echo $"(ansi blue)($path_segment) <||(ansi reset)"
}

# Use nushell functions to define your right and left prompt
let-env PROMPT_COMMAND = {|| create_left_prompt }
let-env PROMPT_COMMAND_RIGHT = {|| create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
let-env PROMPT_INDICATOR = {|| "|:|> "}
let-env PROMPT_INDICATOR_VI_INSERT = {|| "|I|> " }
let-env PROMPT_INDICATOR_VI_NORMAL = {|| "|N|> " }
let-env PROMPT_MULTILINE_INDICATOR = {|| "|::: "}

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
let-env ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
let-env NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
let-env NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# let-env PATH = ($env.PATH | split row (char esep) | prepend '/some/path')
