{
  description = "Prince Bett config";
  
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/release-22.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs"; 

    zig.url = "github:mitchellh/zig-overlay";
    hyprland.url = "github:hyprwm/Hyprland";
  };

  outputs = { nixpkgs,hyprland,zig, ... } @ inputs: {
    nixosConfigurations = {
      nixos = nixpkgs.lib.nixosSystem {
        specialArgs = {inherit inputs;};
        modules = [ 
          hyprland.nixosModules.default
          {programs.hyprland.enable = true;}
          ./system/configuration.nix 
        ];
      };
    };
  };
}
